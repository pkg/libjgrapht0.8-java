#!/bin/sh -e

set -e

DOWNLOADED_FILE=$3

echo "Extracting downloaded archive"
tar zxfv "$DOWNLOADED_FILE"
ROOTDIR=$(tar zft "$DOWNLOADED_FILE" | head -n 1 | cut -d'/' -f1)
cd "$ROOTDIR"
echo "Deleting all JAR files"
find -iname \*.jar -delete
echo "Deleting the precompiled Javadoc documentation"
rm -fvr javadoc
echo "Deleting the touchgraph compatibility code"
rm -fvr src/org/jgrapht/experimental/touchgraph
echo "Repacking archive"
cd ..
tar czfv "$DOWNLOADED_FILE" "$ROOTDIR"
echo "Deleting temporary directory"
rm -fr "$ROOTDIR"

if [ -d ../tarballs ]; then
  echo "Moving the tarballs in the tarballs/ directory"
  mv "$DOWNLOADED_FILE" ../tarballs/
fi

echo "Finished! :-)"

